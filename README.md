# Door Sign

This project involves a Banana Pi displaying the open/closed state of the SPACE.

It can be accessed using a network cable and local ssh, for example:
```
ssh -B enp0s31f6 root@<ip>
```
The necessary credentials should be written on a sign by the Pi.

In `/etc/sysfs.d` there is a file that provides the GPIO ports as virtual files. The script controlling the leds is located in /usr/local/bin and is started via /etc/cron.d at boot.